const newData = [{          //datos enviados por el cliente
    id: 2,
    cdeula: "123456788",
    correo: "kerly@gmail.com"
  }];
  
  
  let clientesDB = [        //simula los datos de la DB
    {
      "id": 2,
      "cdeula": "123554677",
    },
    {
      "id": 3,
      "cdeula": "123456788",
    }
  ]
  
  //funcion para guardar el dato nuevo del cliente
  const guardarDato = (dataCliente) => {
    
    dataCliente.map(data => {    
      clientesDB.push(data);
    });
    
    return clientesDB;
  }
  
  //ejecutar funcion para guardar datos en la base de datos
  if(newData){
    guardarDato(newData);
  }
  console.log(clientesDB);